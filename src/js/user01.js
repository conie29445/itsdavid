import 'page-in.sass'
import Vue from 'libs/vue.min'

var app = new Vue({
    el: '#app__member',
    data: {
        editing: {
            form_data: false,
            form_pw: false
        },
        form_data: {
            has_error: true,
            editing: false,
            name: "Jungle Hung",
            sex: "m", // m: male, f: female, "未填"
            birth: "1995/02/02",
            email: "aaa@gmail.com",
            address: "台中市潭子區中山路"
        },
        form_pw: {
            has_error: true,
            editing: false,
            origin: "",
            new: "",
            checknew: ""
        }
    },
    mounted () {
	},
    methods: {
        gender () {
            if(!this.form_data.sex)
                return this.form_data.sex
            return this.form_data.sex === 'm' ? '男':'女'
        },
        userDataSubmit() {
            this.editing.form_data = !this.editing.form_data;
            // user data 資料修改 submit function
            
        },
        pwSubmit() {
            this.editing.form_pw = !this.editing.form_pw;
            // 密碼 修改 submit function
        }
    }
});
