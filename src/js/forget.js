import 'page-in.sass';
document.addEventListener('DOMContentLoaded', (event) => {
    // 倒數
    let btnSendPW = document.getElementById('js__send__pw');
    btnSendPW.addEventListener('click', function() {
        let timeleft = 60; // 60second
        let self = this;
        self.disabled = true
        self.firstChild.data = timeleft+"秒後可重新發送";
        let countTimer = setInterval(function(){
            if(timeleft <= 0){
                clearInterval(countTimer);
                self.firstChild.data = "重新發送驗證碼"
                self.disabled = false;
                return
            }
            self.firstChild.data = timeleft+"秒後可重新發送";
            timeleft -= 1;
        }, 1000);
    });
});
