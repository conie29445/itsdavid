import 'page-home.sass'
import 'libs/clicker_box'

// owl carousel start ----------------------------------------------
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import 'owl.carousel';
// owl carousel end ----------------------------------------------

$(document).ready(function () {
    
    //-----------------------------------------------
    // banner
    if($('.js__bnslider').length>0) {
        let hasDot = $('.js__bnslider img').length > 1 ? true : false;
        $('.js__bnslider').owlCarousel({
            items: 1,
            animateOut: 'fadeOut',
            dots: hasDot,
            autoplay: true,
            autoplayTimeout: 7000,
            autoHeight:true,
            loop: hasDot,
            nav: false,
            mouseDrag: true
        });
    }
    //-----------------------------------------------

    //-----------------------------------------------
    // idx__snews
    if($('.js__imgslider').length>0) {
        let hasDot = $('.js__imgslider img').length > 1 ? true : false;
        $('.js__imgslider').owlCarousel({
            items: 3,
            margin: 32,
            animateOut: 'fadeOut',
            dots: hasDot,
            autoplay: true,
            autoplayTimeout: 7000,
            autoHeight:true,
            loop: hasDot,
            nav: false,
            responsive:{
                0: {
                    items: 1
                },
                640: {
                    items: 2,
                    margin: 12
                },
                1024: {
                    items: 3,
                    margin: 16
                },
                1480: {
                    margin: 32
                }
            }
        });
    }
    //-----------------------------------------------
});