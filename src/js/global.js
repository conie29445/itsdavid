import '@babel/polyfill'
import 'utilities'
import 'js_components/megamenu'
import 'js_partials/header'
// for resize event
import ResizeObserver from 'resize-observer-polyfill';
// lazy load
import 'lazysizes'
// for scroll
import ScrollMagic from 'scrollmagic/scrollmagic/minified/ScrollMagic.min';
import 'scrollmagic/scrollmagic/minified/plugins/animation.gsap.min';


$(document).ready(function () {
    //-----------------------------------------------
    // 前導動畫 開始
    // document ready , 前導動畫
    const bodyElement = document.querySelector('.body_with_pageloader');
    const preLoadElement = document.querySelector('.pageload');
    if(bodyElement) {
        // 如果有前導動畫
        unloadScrollBars();
        bodyElement.classList.add('is--pageloading')
        function loadAnimation() {
            $('.pageload').addClass('is--loading');
        }
        function animationStart() {
            $('.pageload').addClass('loaded');
        }
        setTimeout(loadAnimation, 100);
        setTimeout(animationStart, 300);
    } else {
        console.log('無前導', preLoadElement)
        if(preLoadElement)
            preLoadElement.style.display = 'none !important';
    }
    // 前導動畫 結束
    //-----------------------------------------------
    
    //-----------------------------------------------
    // header
    // btn mobile click
    header.init();
    header.btn__mobile__mainmenu_click();
    // mobile: header click
    $(".js__mainmenu__list__wrap .dropdown-toggle").clicker_box({
        ACTIVE: "show", // 切換內容
        ATR: "class", // 操作參數
        TARGET: false, // 操作目標 (false,.class,#id)
        RANGE: [false,1279], // 依照解析度啟動 (最小,最大)
    });
    //-----------------------------------------------
    // header scroll
    var headerHeight = document.getElementsByClassName('sticky-wrapper')[0].offsetHeight;
    var stickyWrapper = document.getElementsByClassName('sticky-wrapper')[0];
    var ScrollMagic_controller = new ScrollMagic.Controller();
    stickyWrapper.style.height = headerHeight + "px";
    new ScrollMagic.Scene({
        triggerElement: "#pagemain",
        offset: 0,
        triggerHook: 0
    })
        .on("enter", function () {
            stickyWrapper.style.height = headerHeight + "px";
        })
        .reverse(true)
        .addTo(ScrollMagic_controller);
    new ScrollMagic.Scene({
        triggerElement: "#pagemain",
        offset: 0,
        triggerHook: 0
    })
        .setClassToggle(".js__headerfix", "is--scroll")
        .reverse(true)
        .addTo(new ScrollMagic.Controller());
    new ScrollMagic.Scene({
        triggerElement: "#pagemain",
        offset: headerHeight*1,
        triggerHook: 0
    })
        .setClassToggle(".js__headerfix", "fixed-top")
        .reverse(true)
        .addTo(new ScrollMagic.Controller());
    
    //-----------------------------------------------
    // scroll: gotop btn
    new ScrollMagic.Scene({
        triggerElement: "#pagemain",
        offset: headerHeight*3,
        triggerHook: 0
    })
        .setClassToggle("#js__pagetop", "is--show")
        .reverse(true)
        .addTo(ScrollMagic_controller);
    new ScrollMagic.Scene({
        triggerElement: "#js__mainfooter",
        offset: window.innerHeight*-1,
        triggerHook: 0
    })
        .setClassToggle("#js__pagetop", "is--stick")
        .reverse(true)
        .addTo(ScrollMagic_controller);
    document.getElementById("js__pagetop").addEventListener('click', function(){
        utilities.goTopFunction();
    });

    //-----------------------------------------------
    // megamenu, main menu, resize
    const myObserver = new ResizeObserver((entries, observer) => {
        for (const entry of entries) {
            if(stickyWrapper) {
                stickyWrapper.style.height =  document.getElementsByClassName('mainheader')[0].offsetHeight + "px";
            }
            megamenu.initVariables();
            megamenu.megaMenuShowData();
            header.init();
            header.closeMainMenu();
        }
    });
    const headerElem = document.querySelector('.mainheader');
    myObserver.observe(headerElem);
    //-----------------------------------------------
    
    //-----------------------------------------------
    // aside trigger
    utilities.triggerClass('.js__aside__trigger', 'active', false, false, false )
    //-----------------------------------------------
});

window.onload = function() {
    //-----------------------------------------------
    // page前導動畫
    //-----------------------------------------------
    const bodyElement = document.querySelector('.body_with_pageloader');
    const preLoadElement = document.querySelector('.pageload');
    if(bodyElement) {
        setTimeout(function(){
            bodyElement.classList.remove('is--pageloading')
            bodyElement.classList.add('is--pageloaded')
         }, 900);
        setTimeout(function(){
            reloadScrollBars()
         }, 900);
        //  pageload動畫區塊消失
        setTimeout(function(){
            preLoadElement.style.display = 'none';
         }, 3000);
    } else {
        if(preLoadElement) {
            preLoadElement.style.display = 'none';
        }
    }
    //-----------------------------------------------

};

function reloadScrollBars() {
    document.documentElement.style.overflow = 'auto';  // firefox, chrome
    document.body.scroll = "yes"; // ie only
}

function unloadScrollBars() {
    document.documentElement.style.overflow = 'hidden';  // firefox, chrome
    document.body.scroll = "no"; // ie only
}
