// 前端工具 utilities
import Swal from 'sweetalert2'
import 'libs/clicker_box'

window.utilities = {
	getCookie (name) {
		let eachArray,
			cookieArray = document.cookie.split(';');

		for (let key in cookieArray) {
			eachArray = cookieArray[key].split('=');
			if (name === eachArray[0].trim()) {
				return eachArray[1];
			}
		}
		return null;
	},
	setCookie (name, value) {
		document.cookie = `${name}=${value}; path=/`
	},
	alert(msg, btntext="OK") {
		Swal.fire({
			title: '',
			html:
				`
			<span class="alert__item icon--alert--ok"></span>
			<p class="alert__item alert__title">${msg}</p>
			<p class="text-center"><span id="btn__alert__close" class="d-inline-block text-uppercase btn__alert--confirm btn--base02 pointer">${btntext}</span></p>
			`,
			showConfirmButton: false,
			customClass: {
				actions: 'mt-2 mb-3',
			},
			onBeforeOpen: () => {
				btn__alert__close.addEventListener('click', () => {
					Swal.close()
				})
			}
		})
	},
	alertWithTitle(title, msg) {
		Swal.fire({
			title: '',
			html:
				`
			<p class="alert__item alert__title">${title}</p>
			<p class="alert__item alert__msg">${msg}</p>
			`,
			showConfirmButton: false,
			customClass: {
				actions: 'mt-2 mb-3',
			}
		})
	},
	// click and add class
	triggerClass(triggerName, className, minPx, maxPx, addTarget=false ) {
        var $triggerBtn = $(triggerName);
        if($triggerBtn.length > 0) {
            $triggerBtn.clicker_box({
                ACTIVE: className, // 切換內容
                ATR: "class", // 操作參數
                TARGET: addTarget, // 操作目標 (false,.class,#id)
                RANGE: [minPx,maxPx], // 依照解析度啟動 (最小,最大)
            });
        }
	},
	goTopFunction() {
		jQuery("html, body").animate({scrollTop:0}, 500, 'swing');
	},
	// resize event
	resize_event(baseElement, doEvent) {

	}
};
