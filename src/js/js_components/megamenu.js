module.exports = megamenu = {
    thresholdDesktop:  1280,
    windowsWidth: document.querySelector('.body').clientWidth,
    gridGutter: 15,
    initVariables() {
        this.windowsWidth = document.querySelector('.body').clientWidth;
    },
    //-----------------------------------------------
    // megaMenuShowData
    //-----------------------------------------------
    megaMenuShowData() {
        //Show dropdown on hover only for desktop devices
        //-----------------------------------------------
        let windowWidth = this.windowsWidth;
        let thresholdDesktop = this.thresholdDesktop;
        $('.js__mega__menu__wrap:not(.onclick) .js__mega__menu__nav>.dropdown, .js__mega__menu__wrap:not(.onclick) .dropdown>.dropdown-menu>.dropdown').hover(
        function() {
            if (windowWidth >= thresholdDesktop) {
                $(this).addClass('show');
                $(this).find('>.dropdown-menu').addClass('show');
            }
        }, function() {
            if (windowWidth >= thresholdDesktop) {
                $(this).removeClass('show');
                $(this).find('>.dropdown-menu').removeClass('show');
            }
        });
    },

    //-----------------------------------------------
    // megamenuPosWidth_Wide: position and width for mega-menu--wide
    //-----------------------------------------------
    megamenuPosWidth_Wide() {
        if ($('.js__mega__menu__wrap .mega-menu--wide').length>0 && this.windowsWidth > this.thresholdDesktop - 1) {
            var $dropdownMenu = $('.mega-menu--wide > .dropdown-menu'); // target
            var headerSecondLeft = parseInt($dropdownMenu.closest('.dropdown').offset().left),
            headerFirstLeft = parseInt($('.js__mega__menu__wrap').offset().left),
            megaMenuLeftPosition = headerFirstLeft - headerSecondLeft;
            $dropdownMenu.css('left', megaMenuLeftPosition + 'px');
            console.log('left = ', megaMenuLeftPosition);
            $(window).resize(function() {
                var headerSecondLeft = parseInt($('.js__mega__menu__wrap').offset().left),
                headerFirstLeft = parseInt($('.js__mainmenu__wrap').offset().left),
                megaMenuLeftPosition = headerFirstLeft - headerSecondLeft;
                $dropdownMenu.css('left', megaMenuLeftPosition + 'px');
                console.log('resize, left = ', megaMenuLeftPosition);
            });
        }
    },
    //-----------------------------------------------
    // megamenuPosWidth_Full: position and width for mega-menu--wide
    //-----------------------------------------------
    megamenuPosWidth_Full() {
        //Mega menu full width
        if ($('.js__mega__menu__wrap .mega-menu--full').length>0 && this.windowsWidth > this.thresholdDesktop - 1) {
            var $dropdownMenu = $('.mega-menu--full > .dropdown-menu'); // target
            var headerSecondLeft = parseInt($('.js__mega__menu__wrap').offset().left + this.gridGutter),
            headerFirstLeft = parseInt($('.js__mainmenu__wrap').offset().left),
            megaMenuLeftPosition = headerFirstLeft - headerSecondLeft,
            megaMenuWidth = parseInt($('.mainheader').width());
            $dropdownMenu.css('left', megaMenuLeftPosition + 'px');
            $dropdownMenu.css('width', megaMenuWidth + 'px');
            $(window).resize(function() {
                var headerSecondLeft = parseInt($('.js__mega__menu__wrap').offset().left + this.gridGutter),
                headerFirstLeft = parseInt($('.js__mainmenu__wrap').offset().left),
                megaMenuLeftPosition = headerFirstLeft - headerSecondLeft,
                megaMenuWidth = parseInt($('.mainheader').width());
                var $dropdownMenu = $('.mega-menu--full > .dropdown-menu');
                $dropdownMenu.css('left', megaMenuLeftPosition + 'px');
                $dropdownMenu.css('width', megaMenuWidth + 'px');
            });
        }
    }
    //-----------------------------------------------
};