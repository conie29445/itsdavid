import 'page-in.sass'

// owl carousel start ----------------------------------------------
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import 'owl.carousel';
// owl carousel end ----------------------------------------------
$(document).ready(function () {
    //-----------------------------------------------
    // idx__snews
    if($('.js__itemslider').length>0) {
        let hasDot = $('.js__itemslider .item').length > 1 ? true : false;
        $('.js__itemslider').owlCarousel({
            items: 3,
            margin: 32,
            animateOut: 'fadeOut',
            dots: hasDot,
            autoplay: false,
            // autoplayTimeout: 7000,
            autoHeight:true,
            loop: hasDot,
            nav: false,
            responsive:{
                0: {
                    items: 1
                },
                1024: {
                    items: 2,
                    margin: 12
                },
                1480: {
                    margin: 32
                }
            }
        });
    }
    //-----------------------------------------------  
});