import 'page-in.sass'

$(document).ready(function () {
    //-----------------------------------------------
    let boxwraps = document.querySelectorAll('.detail__boxes__wrap');
    let buttonsOpenDetail = document.querySelectorAll('.btn__base');
    let buttonsClose = document.querySelectorAll('.js__btn__close');
    let triggerCloseBox = document.getElementById('js__close__all__box');
    for(let i = 0; i < buttonsOpenDetail.length; i++) {
        // open
        buttonsOpenDetail[i].addEventListener('click', function(){
            if(!boxwraps[i].classList.contains('show--detail')) {
                boxwraps[i].classList.add('show--detail');
            }
            triggerCloseBox.classList.add('detail--open');
        });
    } 
    for(let i = 0; i < buttonsClose.length; i++) {
        // close
        buttonsClose[i].addEventListener('click', function(){
            closeDetailBoxs();
        });
    } 
    triggerCloseBox.addEventListener('click', function(){
        closeDetailBoxs();
    });
    function closeDetailBoxs() {
        if(boxwraps.length>0) {
            for(let i = 0; i < boxwraps.length; i++) {
                if(boxwraps[i].classList.contains('show--detail'))
                    boxwraps[i].classList.remove('show--detail')
            }
        }
        triggerCloseBox.classList.remove('detail--open');
    }
        
    //-----------------------------------------------  
});