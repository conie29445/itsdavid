module.exports = header = {
    //-----------------------------------------------
    // btn__mobile__mainmenu_click
    //-----------------------------------------------
    mobileMainMenuButton: document.querySelector('.js__btn__mobile__mainmenu'),
    bodyElement: document.querySelector('.body'),
    init(){

    },
    btn__mobile__mainmenu_click() {
        // this.mobileMainMenuButton = document.querySelector('.js__btn__mobile__mainmenu');
        this.mobileMainMenuButton.addEventListener('click', () => {
            if(!this.mobileMainMenuButton.classList.contains('btn__mainmenu__mobile--active')) {
                this.openMainMenu();
            } else {
                this.closeMainMenu();
            }
        });
    },
    closeMainMenu() {
        this.mobileMainMenuButton.classList.remove('btn__mainmenu__mobile--active')
        this.bodyElement.classList.remove('mainmenu--opened');
    },
    openMainMenu() {
        this.mobileMainMenuButton.classList.add('btn__mainmenu__mobile--active')
        if (!this.bodyElement.classList.contains('mainmenu--opened')) {
            this.bodyElement.classList.add('mainmenu--opened')
        }
    },
    closeSubMenus() {
        let toggleBtns = ths.bodyElement.querySelector('.js__mega__menu__nav').querySelectorAll('.dropdown');
        let dropdownMenus = ths.bodyElement.querySelector('.js__mega__menu__nav').querySelectorAll('.dropdown-menu');
        for(let i = 0; i < toggleBtns.length; i++) {
            if (toggleBtns[i].classList.contains('show')) {
                toggleBtns[i].classList.remove('show')
            }
        }
        for(let i = 0; i < dropdownMenus.length; i++) {
            if (dropdownMenus[i].classList.contains('show')) {
                dropdownMenus[i].classList.remove('show')
            }
        }
    }
};