import 'page-in.sass';
document.addEventListener('DOMContentLoaded', (event) => {
    // step
    let currentStep = document.querySelector('.step__item.current')
    if(currentStep) {
        setTimeout(function(){
            currentStep.classList.add('active');
        }, 300);
    }

    // finish pic
    let finishpic = document.getElementById('js__register__finish__pic')
    setTimeout(function(){
        finishpic.classList.add('active');
    }, 300);
});